<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\MinkExtension\Context\RawMinkContext;

require_once __DIR__ . '/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';
/**
 * Defines application features from the specific context.
 */
class CommandLineContext extends RawMinkContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        /*        $driver = new FirefoxDriver();
                $driver.manage().window().maximize();*/
    }

    /**
     * @Given I logged in already
     */
    public function iLoggedInAlready()
    {
        $this->visitPath('/administrator/login');
        $this->getSession()->getPage()->fillField('username', 'admin');
        $this->getSession()->getPage()->fillField('password', 'Asdzxc123!');
        $this->getSession()->getPage()->pressButton('submit');
    }
    /**
     * @When I wait for :arg1 seconds
     */
    public function iWaitForSeconds($arg1)
    {
        $this->getSession()->wait(5000, "$('.suggestions-results').children().length > 0");
    }

    /**
     * @When I fill id :id with :value
     */
    public function iFillIdWith2($id, $value)
    {
        $valueBox = $this->getSession()->getPage()->find('css', $value);
        assertNotNull($valueBox, "the id is not found");
        $valueBox->setValue($value);
    }
    /**
     * @When I fill name :name with :value
     */
    public function iFillNameWith2($name, $value)
    {
        $valueBox = $this->getSession()->getPage()->find('css', '[name=' . $name . ']');
        assertNotNull($valueBox, "the name is not found");
        $valueBox->setValue($value);
    }



    /**
     * @When I break
     */
    public function iBreak2()
    {
        fwrite(STDOUT, "\033[s    \033[93m[Breakpoint] Press \033[1;93m[RETURN]\033[0;93m to continue...\033[0m");
        while (fgets(STDIN, 1024) == '') {
        }
        fwrite(STDOUT, "\033[u");
        return;
    }
    /**
     * Saving a screenshot
     *
     * @When I save a screenshot in :filename
     */
    public function iSaveAScreenshotIn($filename)
    {
        sleep(1);
        $this->saveScreenshot($filename, $this->screenshotDir);
    }
}