Feature: transaction report should display the transaction records
  In order to check the transaction
  As a user or administrator
  I need to see the table of transaction records

  Background:
    Given I logged in already
    And I should see "Dashboard"

  @javascript
  Scenario: transaction list page
    Given I go to "/administrator/transaction"
    When I follow "Show Advanced Search"
    And I select "25" from "Result Per Page"
    And I fill in "from_date" with "2017-7-26 00:00:00"
    And I fill in "to_date" with "2017-12-21 00:00:00"
    #And I follow "+ add search field (logic : AND)"
    #And I wait for "5" seconds
    And I break
    And I press "Search"
    Then I should see "Save as CSV"





