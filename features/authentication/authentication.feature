Feature: Authentication
  In order to login the management panel
  As a admin user
  I need to be able to login and logout

  Scenario: Login
    Given I am on "/"
    When I fill in "username" with "admin"
    And I fill in "password" with "Asdzxc123!"
    And I press "submit"
    Then I should see "Dashboard"